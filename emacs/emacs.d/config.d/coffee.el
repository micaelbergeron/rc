(custom-set-variables '(coffee-tab-width 2))

(add-to-list 'auto-mode-alist
             '("\\.csjx" . (lambda () (coffee-mode))))
