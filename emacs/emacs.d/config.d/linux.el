;; GNU/Linux specific configuration

;; (set-frame-font "-FBI -Input Mono Narrow-normal-normal-semicondensed-*-12-*-*-*-m-0-iso10646-1" nil t)
(setq cursor-type 'bar)
(setq sml/theme 'smart-mode-line-powerline)
(sml/setup)
(add-to-list 'sml/replacer-regexp-list '("^~/git" "git:") t)

