;; General editor style

(defun my-prog-mode-hook ()
  "My `prog-mode` hook."
  (interactive)
  (linum-mode)
  (flycheck-mode)
  ;; (highlight-indent-guides-mode)
  (yafolding-mode)
  (yas-minor-mode)
  
  (setq flycheck-check-syntax-automatically '(mode-enabled save))
  ;; (setq highlight-indent-guides-method 'character)
  ;; (setq highlight-indent-guides-character ?\¦)
)

(add-hook 'prog-mode-hook 'my-prog-mode-hook)

(defun align-colons (beg end)
  (interactive "r")
  (align-regexp beg end ":\\(\\s-*\\)" 1 1 t))

(defun align-hash (beg end)
  (interactive "r")
  (align-regexp beg end "\\(\\s-*\\)\=\>\\(\\s-*\\)" 1 1 t))

(defun align-hash (beg end)
  (interactive "r")
  (align-regexp beg end "\\(\\s-*\\)\=\>\\(\\s-*\\)" 1 1 t))

(defun align-commas (beg end)
  (interactive "r")
  (align-regexp beg end ",\\(\\s-*\\)" 1 1 t))
