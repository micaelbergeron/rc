;; Loads ruby mode when a .rb file is opened.
(autoload 'ruby-mode "ruby-mode" "Major mode for editing ruby scripts." t)
(setq auto-mode-alist (cons '(".rb$" . ruby-mode) auto-mode-alist))
(setq auto-mode-alist (cons '(".rhtml$" . html-mode) auto-mode-alist))
(setq auto-mode-alist (cons '(".html.erb$" . ruby-mode) auto-mode-alist))

(defun my-ruby-mode-hook ()
  "My ruby mode hooks."
  (interactive)
  (rinari-minor-mode t)
  (flycheck-mode t)
  (eldoc-mode t)
  (local-set-key (kbd "C-c r a") 'rubocop-autocorrect-current-file)
  )

(add-hook 'ruby-mode-hook 'my-ruby-mode-hook)
