(add-hook 'python-mode-hook
	  (lambda ()
	    (setq-default indent-tabs-mode nil)
	    (setq-default python-indent-offset 4)
	    (setq-default python-indent-guess-indent-offset nil)
	    (add-to-list 'write-file-functions 'delete-trailing-whitespace)))
